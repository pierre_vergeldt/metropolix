
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
    <div class="pull-left image">
        <img src="{{asset('/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
    </div>
    <div class="pull-left info">
        <p>{{ Auth::user()->name }}</p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div> 
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li class="header">Hoofdmenu</li>   
    <li class="treeview active">
      <a href="#">
        <i class="fa fa-home"></i>
        <span>Ondernemingen</span>
        <!-- <span class="label label-primary pull-right">4</span>-->
      </a>
      <ul class="treeview-menu ">
        <li><a href="#"><i class="fa fa-circle-o"></i> Toevoegen</a></li> 
        <li class="active"><a href="#"><i class="fa fa-circle-o"></i> Overzicht</a></li> 
        <li><a href="#"><i class="fa fa-circle-o"></i> Archief</a></li> 
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-users"></i>
        <span>Personen</span>
        <!-- <span class="label label-primary pull-right">4</span>-->
      </a>
      <ul class="treeview-menu">
        <li><a href="#"><i class="fa fa-circle-o"></i> Toevoegen</a></li> 
        <li><a href="#"><i class="fa fa-circle-o"></i> Overzicht</a></li> 
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-edit"></i>
        <span>Taken</span>
        <!-- <span class="label label-primary pull-right">4</span>-->
      </a>
      <ul class="treeview-menu">
        <li><a href="#"><i class="fa fa-circle-o"></i> Toevoegen</a></li> 
        <li><a href="#"><i class="fa fa-circle-o"></i> Overzicht</a></li> 
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-calendar-o"></i>
        <span>Memo's</span>
        <!-- <span class="label label-primary pull-right">4</span>-->
      </a>
      <ul class="treeview-menu">
        <li><a href="#"><i class="fa fa-circle-o"></i> Toevoegen</a></li> 
        <li><a href="#"><i class="fa fa-circle-o"></i> Overzicht</a></li> 
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-calendar-check-o"></i>
        <span>Memo's Controle</span>
        <!-- <span class="label label-primary pull-right">4</span>-->
      </a>
      <ul class="treeview-menu">
        <li><a href="#"><i class="fa fa-circle-o"></i> Toevoegen</a></li> 
        <li><a href="#"><i class="fa fa-circle-o"></i> Overzicht</a></li> 
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-calendar-plus-o"></i>
        <span>Memo's Koenen</span>
        <!-- <span class="label label-primary pull-right">4</span>-->
      </a>
      <ul class="treeview-menu">
        <li><a href="#"><i class="fa fa-circle-o"></i> Toevoegen</a></li> 
        <li><a href="#"><i class="fa fa-circle-o"></i> Overzicht</a></li> 
      </ul>
    </li> 
    <li class="treeview">
      <a href="#">
        <i class="fa fa-envelope"></i>
        <span>Brieven</span>
        <!-- <span class="label label-primary pull-right">4</span>-->
      </a>
      <ul class="treeview-menu">
        <li><a href="#"><i class="fa fa-circle-o"></i> Toevoegen</a></li> 
        <li><a href="#"><i class="fa fa-circle-o"></i> Overzicht</a></li> 
      </ul>
    </li> 
    <li class="treeview">
      <a href="#">
        <i class="fa fa-phone"></i>
        <span>Telefoonnotities</span>
        <!-- <span class="label label-primary pull-right">4</span>-->
      </a>
      <ul class="treeview-menu">
        <li><a href="#"><i class="fa fa-circle-o"></i> Toevoegen</a></li> 
        <li><a href="#"><i class="fa fa-circle-o"></i> Overzicht</a></li> 
      </ul>
    </li> 
    <li class="treeview">
      <a href="#">
        <i class="fa fa-calendar"></i>
        <span>Schema's</span>
        <!-- <span class="label label-primary pull-right">4</span>-->
      </a>
      <ul class="treeview-menu">
        <li><a href="#"><i class="fa fa-circle-o"></i> Toevoegen</a></li> 
        <li><a href="#"><i class="fa fa-circle-o"></i> Overzicht</a></li> 
      </ul>
    </li> 
    <li class="treeview">
      <a href="#">
        <i class="fa fa-book"></i>
        <span>Handleidingen</span> 
      </a> 
    </li> 
    <li class="treeview">
      <a href="#">
        <i class="fa fa-wrench"></i>
        <span>Beheer</span> 
      </a> 
    </li>
  </ul>
</section>
<!-- /.sidebar -->
</aside>
